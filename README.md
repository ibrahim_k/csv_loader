### Overview
1. CSV Loader is a web application to upload your CSV data. Here, data will be stored in disk as pickle.
2. Stored Data will be retrieved by using API Endpoints(mentioned below).

### Technologies
    * Python 3.5
    * Flask (Web Framework)
    * Nginx (Web Server)
    * UWSGI (App Server)
    * Docker


### Project setup instructions[Docker]:
 1. Install [Docker][1]
 2. Install [Docker Compose][2]
 3. Clone the repo and switch to repository's directory.
 3. Run Docker Build `docker-compose build`.
 4. Run Docker Up `docker-compose up -d`.
 5. Open the url(http://localhost:5000) in browser to load the application.
 

### Project setup instructions[VirtualEnv]:
 1. Create an `virtual environment` using python 3.7.
 2. Install requirements using the command `pip install -r requirements.txt`.
 3. Run the command `python3 app/server.py`.
 4. Open the url(http://localhost:5000) in browser to load the application.


### API Endpoints
1. URI: http://{server}:{port}/{key}

    Valid Key Response 
    ```
    { 
        "key": {key},
        "value": {value}
    }
    ```

    InValid Key Response
    ```
    { 
        "key": {key},
        "value": "Invalid Key"
    }
    ```

2. URI: http://{server}:{port}/all

    ```
    { 
        "key": "value",
        "key1": "value",
        "key2": "value"
    }
    ```

### Run Test Cases

1. Run the command `python3 test_csv_loader.py`.

### References 
 
 1. https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-docker-ce
 
 2. https://docs.docker.com/compose/install/#install-compose
 
 3. Use Command `docker ps` to view active containers.