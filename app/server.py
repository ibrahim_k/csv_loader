import os
import csv
import pickle
import json

from flask import Flask, render_template, request, Response, flash


app = Flask(__name__, template_folder='templates')

# pickle file(/tmp/outfile) used for saving csv data as python objects
app.config['OUT_FILE_LOC'] = '/tmp/outfile' 


@app.route('/', methods=['GET', 'POST'])
def home():
    context = {}
    if request.method == "POST":
        try:
            uploaded_file = request.files.get('upload')
            if uploaded_file.filename.lower()[-4:] != ".csv":
                raise Exception("Invalid File Formats Uploaded, Valid formats are .csv")
            decoded_file = uploaded_file.read().decode('utf-8').splitlines()
            reader = csv.DictReader(decoded_file)
            data = {}
            for row in reader:
                if row['key'] == "":
                    break
                data[row['key']] = row['value']
            with open(app.config['OUT_FILE_LOC'], 'wb') as outfile:
                pickle.dump(data, outfile)
                context = {
                    'success_msg': True,
                    'error_msg': False
                } 
        except Exception as e:
            context = {
                'success_msg': False,
                'error_msg': True,
                'errors': str(e)
            }
    return render_template('index.html', **context)


@app.route('/<key>', methods=['GET'])
def search(key):
    status_code = 200
    if os.path.exists(app.config['OUT_FILE_LOC']):
        with open(app.config['OUT_FILE_LOC'], 'rb') as infile:
            data = pickle.load(infile)
            if key == "all":
                response = data
            else:
                response = {
                    "key": key,
                    "value": data.get(key) if data.get(key) else "Invalid Key"
                }
    else:
        response = {
            "message": "Data does n't exists"
        }
        status_code = 400
    return Response(json.dumps(response), status=status_code)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)