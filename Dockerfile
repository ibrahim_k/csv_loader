FROM python:3.5

RUN apt-get update
RUN apt-get install -y --allow-unauthenticated --no-install-recommends \
        libatlas-base-dev gfortran nginx supervisor

RUN pip3 install uwsgi

COPY ./requirements.txt /project/requirements.txt

RUN pip3 install -r /project/requirements.txt

RUN useradd --no-create-home nginx

RUN rm /etc/nginx/sites-enabled/default
RUN rm -r /root/.cache

COPY deployment/nginx.conf /etc/nginx/
COPY deployment/flask-site-nginx.conf /etc/nginx/conf.d/
COPY deployment/uwsgi.ini /etc/uwsgi/
COPY deployment/supervisord.conf /etc/

COPY /app /project

WORKDIR /project

CMD ["/usr/bin/supervisord"]
