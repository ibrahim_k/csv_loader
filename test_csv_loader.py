import unittest
from io import BytesIO

from app.server import app


class CSVUploaderTestCase(unittest.TestCase):

    # ensure the application setup correctly
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    # ensure the application loads correctly
    def test_index_loads(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertTrue(b'CSV Uploader' in response.data)

    # ensure the application uploading CSV correctly
    def test_upload_csv(self):
        tester = app.test_client(self)
        data = dict(
            file=(BytesIO(b'my file contents'), "sample.corpus.CSV"),
        )

        response = tester.post('/', content_type='multipart/form-data', data=data)
        self.assertEqual(response.status_code, 200)

    # ensure the response of api if no uploaded file exists
    def test_no_csv_uploaded(self):
        test_key_val = "one"
        app.config['OUT_FILE_LOC'] = ''
        tester = app.test_client(self)
        response = tester.get('/' + test_key_val)
        self.assertEqual(response.status_code, 400)

    # ensure the application returns correct response for valid key
    def test_api_search_key_exists(self):
        test_key_val = "one"
        tester = app.test_client(self)
        app.config['OUT_FILE_LOC'] = 'test.outfile'
        response = tester.get('/' + test_key_val)
        self.assertEqual(response.status_code, 200)

    # ensure the application returns correct response for invalid key
    def test_api_search_key_not_exists(self):
        test_key_val = "none"
        tester = app.test_client(self)
        app.config['OUT_FILE_LOC'] = 'test.outfile'
        response = tester.get('/' + test_key_val)
        self.assertTrue(b'Invalid Key', response.data)


if __name__ == "__main__":
    unittest.main()
